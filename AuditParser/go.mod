module bitbucket.org/bharrelldev/devops-utilities/AuditParser

go 1.14

require (
	cloud.google.com/go/storage v1.12.0
	github.com/mohae/struct2csv v0.0.0-20151122200941-e72239694eae
	github.com/urfave/cli/v2 v2.2.0
)
