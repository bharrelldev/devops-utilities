package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"github.com/mohae/struct2csv"
	"cloud.google.com/go/storage"
	"github.com/urfave/cli/v2"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type Assets []Payload

func (m Assets) Iterate() <-chan Payload {

	payloadChan := make(chan Payload)

	go func() {
		for _, payload := range m {
			payloadChan <- payload
		}
		close(payloadChan)
	}()

	return payloadChan

}

func transferToJson(fi os.FileInfo) error {
	var jsonFile string


	baseName := fi.Name()

	if !strings.Contains(baseName, "."){
		jsonFile = strings.Join([]string{baseName, "json"}, ".")
	}

	if strings.Contains(baseName, ".txt"){
		jsonFile = strings.ReplaceAll(baseName, ".txt", ".json")
	}


	fmt.Println(jsonFile)

	f, err := os.Open("audit/" + baseName)
	if err != nil {
		return err
	}

	defer f.Close()

	fInfo, err := f.Stat()
	if err != nil {
		return err
	}

	buf := make([]byte, fInfo.Size())

	offset, err := f.Read(buf)
	if err != nil {
		return err
	}

	content := buf[:offset]

	log.Printf("changing %s to json\n", baseName)
	outFile, err := os.Create("audit_json/" + jsonFile)
	if err != nil {
		return err
	}

	if _, err := outFile.Write(content); err != nil {
		return err
	}

	return nil
}

func ReadJsonFiles(jsonFile os.FileInfo) (Assets, error) {
	var asset Assets
	auditJson, err := ioutil.ReadFile("audit_json/" + jsonFile.Name())
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(auditJson, &asset); err != nil {
		return nil, err
	}

	return asset, nil

}

func main() {

	app := cli.NewApp()
	app.Usage = "audit-cli <options>"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name: "dir",
			Required: true,


		},
	}
	app.Action = func(c *cli.Context) error {

		files, err := filepath.Glob(strings.Join([]string{c.String("dir"), "*" }, "/"))
		if err != nil{
			return fmt.Errorf("no match found for file")
		}

		if err := UploadToGCS(files); err != nil{
			return fmt.Errorf("error uploading file to gcs %v", err)
		}

		return nil
	}

	if err := app.Run(os.Args); err != nil{
		log.Fatalf("error launching app %v", err)
	}


}

func UploadToGCS(fileList []string) error {
	folder := time.Now().Format(time.RFC3339Nano)

	client, err := storage.NewClient(context.Background())
	if err != nil{
		return fmt.Errorf("error cannot instantiate new client %v", err)
	}

	fmt.Println(client)

	for _, file := range fileList{
		bucket := client.Bucket("insight-inventory-bucket")
		f, err := ioutil.ReadFile(file)
		if err != nil{
			return fmt.Errorf("error cannot open file %v", err)
		}
		bucketWriter := bucket.Object(fmt.Sprintf("%s/%s.json", folder,
			strings.ReplaceAll(file, "audit\\","")) ).
			NewWriter(context.Background())


		if _, err := bucketWriter.Write(f); err != nil{
			fmt.Println(err)
			return fmt.Errorf("error writing to bucket %v", err)
		}

		if err := bucketWriter.Close(); err != nil{
			return fmt.Errorf("close failed %v", err)
		}
	}

	return nil
}


func MarshalCSV(csvInfo CSVFileInfo)  error {

	s2csv := struct2csv.New()
	enc, err := s2csv.Marshal(csvInfo.Assets)
	if err != nil {
		return  err
	}

	if _, err := os.Stat(csvInfo.OutDir); os.IsNotExist(err){
		if err := os.Mkdir(csvInfo.OutDir, os.ModePerm); err != nil{
			return  err

		}
	}


	f, err := os.Create(filepath.Join(csvInfo.OutDir, csvInfo.FileName))
	if err != nil {
		return err
	}

	w := csv.NewWriter(f)
	if err := w.WriteAll(enc); err != nil {
		return err
	}

	return  nil

}
