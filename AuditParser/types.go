package main

type CSVFileInfo struct {
	FileName string
	OutDir string
	Assets   Assets
}

type Payload struct {
	Asset Asset `json:"asset"`
}

type Asset struct {
	Name                     string                   `json:"name"`
	SecurityCenterProperties SecurityCenterProperties `json:"securityCenterProperties"`
	ResourceProperties       ResourceProperties       `json:"resourceProperties"`
	SecurityMarks            SecurityMarks            `json:"securityMarks"`
	CreateTime               string                   `json:"createTime"`
	UpdateTime               string                   `json:"updateTime"`
	IamPolicy                IamPolicy                `json:"iamPolicy"`
}

type SecurityCenterProperties struct {
	ResourceName               string   `json:"resourceName"`
	ResourceType               string   `json:"resourceType"`
	ResourceParent             string   `json:"resourceParent"`
	ResourceProject            string   `json:"resourceProject"`
	ResourceOwners             []string `json:"resourceOwners"`
	ResourceDisplayName        string   `json:"resourceDisplayName"`
	ResourceParentDisplayName  string   `json:"resourceParentDisplay_name"`
	ResourceProjectDisplayName string   `json:"resourceProjectDisplayName"`
}

type ResourceProperties struct {
	Address         string `json:"address"`
	CreateTimestamp string `json:"createTimestamp"`
	Purpose         string `json:"purpose"`
	Subnetwork      string `json:"subnetwork"`
	Description     string `json:"description"`
	SelfLink        string `json:"selfLink"`
	AddressType     string `json:"addressType"`
	NetworkTier     string `json:"networkTier"`
	Users           string `json:"users"`
	Status          string `json:"status"`
	Region          string `json:"region"`
	Id              string `json:"id"`
	Name            string `json:"name"`
	LifeCycleState  string `json:"lifeCycleState"`
	Parent          string `json:"parent"`
	ProjectID       string `json:"projectId"`
	CreateTime      string `json:"createTime"`
	ProjectNumber   string `json:"projectNumber"`
}

type SecurityMarks struct {
	Name string `json:"name"`
}

type IamPolicy struct {
	PolicyBlob string `json:"policy_blob"`
}
