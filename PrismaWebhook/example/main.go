package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	opsGenieCreateRequest := OpsGeneieCreateAlertRequest{
		Message:  "alert created by api ",
		Priority: "P3",
	}

	opsGenieJson, err := json.Marshal(opsGenieCreateRequest)
	if err != nil {
		fmt.Println(err)
		return
	}

	req, err := http.NewRequest(http.MethodPost,
		"https://api.opsgenie.com/v2/alerts",
		bytes.NewBuffer(opsGenieJson))
	if err != nil {
		fmt.Println(err)
		return
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("GenieKey %s", "83559b2e-86df-4e8f-ae21-29d89ec7e0cc"))

	client := http.DefaultClient

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}

	f, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	fmt.Println(string(f))

	defer resp.Body.Close()

}
