package main

type OpsGeneieCreateAlertRequest struct {
	Message     string       `json:"message"`
	Alias       string       `json:",omitempty"`
	Description string       `json:"description"`
	Responders  []*Responder `json:"responders,omitempty"`
	VisibleTo   []*VisibleTo `json:"visibleTo,omitempty"`
	Actions     []string     `json:"actions,omitempty"`
	Tags        []string     `json:"tags,omitempty"`
	Details     *Details     `json:",omitempty"`
	Entity      string       `json:",omitempty"`
	Priority    string       `json:"priority,omitempty"`
}

type Responder struct {
	Id       string `json:"id,omitempty"`
	Name     string `json:",omitempty"`
	Username string `json:"username,omitempty"`
	Type     string `json:"type"`
}

type VisibleTo struct {
	Id       string `json:"id,omitempty"`
	Name     string `json:"name,omitempty"`
	Username string `json:"username,omitempty"`
	Type     string `json:"type"`
}

type Details struct {
	Key1 string `json:"key1"`
	Key2 string `json:"key2"`
}
