module bitbucket.org/bharrelldev/devops-utilities/PrismaWebhook

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	github.com/urfave/cli/v2 v2.2.0
)
