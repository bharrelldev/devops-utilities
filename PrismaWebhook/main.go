package main

import (
	"github.com/urfave/cli/v2"
	"log"
	"os"
)

func main() {

	app := cli.NewApp()
	app.Name = "prisma-hook"
	app.Description = "webhook for prisma cloud alerts"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:     "api-key",
			Required: true,
			EnvVars: []string{
				"GENIE_API_KEY",
			},
		},
		&cli.StringFlag{
			Name:     "api-url",
			Required: true,
			EnvVars: []string{
				"GENIE_API_URL",
			},
		},
	}

	app.Commands = []*cli.Command{
		{
			Name: "prisma",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:    "port",
					Aliases: []string{"p"},
					Usage:   "port for hook service",
					EnvVars: []string{
						"PRISMA_HOOK_PORT",
					},
					Required: true,
					Value:    "3000",
				},
			},
			Subcommands: []*cli.Command{
				{
					Name:   "start",
					Action: start,
				},
			},
		},
		{
			Name: "ops-genie",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:  "port",
					Usage: "port for opsgenie hook service",
					EnvVars: []string{
						"OPSGENIE_HOOK_PORT",
					},
					Required: true,
					Value:    "5000",
				},
			},
			Subcommands: []*cli.Command{
				{
					Name:   "start",
					Action: opsGenieStart,
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
