package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/urfave/cli/v2"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

func opsGenieStart(c *cli.Context) error {

	apiUrl := c.String("api-url")
	apiKey := c.String("api-key")

	errChan := make(chan error, 1)
	sigChan := make(chan os.Signal, 1)

	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	go func() {
		if err := startGenieHook(apiUrl, apiKey, c.String("port")); err != nil {
			errChan <- err
			return
		}
	}()

	select {
	case err := <-errChan:
		if err != nil {
			return fmt.Errorf("error occured when initializing server %v", err)
		}
	}

	return nil
}

func startGenieHook(apiURL, apiKey, port string) error {
	r := mux.NewRouter()
	r.HandleFunc("/opsGenie", opsGenieHandler(apiKey, apiURL)).Methods(http.MethodPost)

	s := http.Server{
		Addr:    fmt.Sprintf(":%s", port),
		Handler: r,
	}

	return s.ListenAndServe()
}

func opsGenieHandler(apiKey, apiURL string) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var opsGenieResp OpsGenieRequest

		if err := json.NewDecoder(r.Body).Decode(&opsGenieResp); err != nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		alertId := opsGenieResp.Alert.AlertId


		if strings.Contains(opsGenieResp.Alert.Message, "just started building"){

			log.Printf("alert_id=%s is being auto deleted", alertId)
			req, err := http.NewRequest(http.MethodDelete,
				fmt.Sprintf("%s/%s/%s", apiURL, "alerts", alertId), nil)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			req.Header.Add("Content-Type", "application/json")
			req.Header.Add("Authorization", fmt.Sprintf("GenieKey %s", apiKey))

			client := http.DefaultClient

			resp, err := client.Do(req)
			if err != nil{
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			f, err := ioutil.ReadAll(resp.Body)
			if err != nil{
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}


			if _, err := w.Write(f); err != nil{
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			 w.WriteHeader(http.StatusOK)
			return

		}

		if _, err := w.Write([]byte("alert doesnt' require filtering"));err != nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return

		}


		return




	}

}
