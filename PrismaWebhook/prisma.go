package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/urfave/cli/v2"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
)

func start(c *cli.Context) error {

	apiUrl := c.String("api-url")
	apiKey := c.String("api-key")

	fmt.Println(apiUrl)

	errChan := make(chan error, 1)
	sigChan := make(chan os.Signal, 1)

	signal.Notify(sigChan, os.Interrupt)

	go func() {
		if err := startPrismaHook(apiUrl, apiKey, c.String("port")); err != nil {
			errChan <- err
			return
		}
	}()

	select {
	case err := <-errChan:
		if err != nil {
			return fmt.Errorf("error occured when initializing server %v", err)
		}
	}

	return nil

}

func startPrismaHook(opsGenieURL, opsGenieKey, hookPort string) error {
	router := mux.NewRouter()
	router.HandleFunc("/prisma", prismaHook(opsGenieKey, opsGenieURL))

	s := http.Server{
		Addr:    fmt.Sprintf(":%s", hookPort),
		Handler: router,
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", hookPort))
	if err != nil {
		return fmt.Errorf("error initalizing listener %v", err)
	}

	return s.Serve(lis)

}

func prismaHook(opsGenieAPIKey, opsGenieURL string) func(w http.ResponseWriter, r *http.Request) {

	return func(w http.ResponseWriter, r *http.Request) {
		var pa PrismaAlert

		if err := json.NewDecoder(r.Body).Decode(&pa); err != nil {
			fmt.Println(err)
			if !strings.Contains(err.Error(), "unmarshal array") {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.WriteHeader(http.StatusOK)
			return
		}

		if strings.ToLower(pa.Severity) == "high" {
			log.Println("medium alert detected")
			opsGenieCreateRequest := OpsGeneieCreateAlertRequest{
				Message: fmt.Sprintf("%s alert from Prisma Cloud\nAlert Msg: %s\n Env: %s",
					pa.Severity, pa.PolicyName, pa.AccountId),
				Priority: "P3",
			}

			opsGenieJson, err := json.Marshal(opsGenieCreateRequest)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			req, err := http.NewRequest(http.MethodPost,
				fmt.Sprintf("%s/%s", opsGenieURL, "alerts"),
				bytes.NewBuffer(opsGenieJson))
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			req.Header.Add("Content-Type", "application/json")
			req.Header.Add("Authorization", fmt.Sprintf("GenieKey %s", opsGenieAPIKey))

			client := http.DefaultClient

			resp, err := client.Do(req)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			defer resp.Body.Close()

			if resp.StatusCode > 300 {
				http.Error(w, errors.New("create alert request failed").Error(), resp.StatusCode)
				return

			}

		}

		w.WriteHeader(http.StatusOK)

	}

}
