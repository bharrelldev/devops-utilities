package main

type PrismaAlert struct {
	Severity             string               `json:"severity"`
	ResourceId           string               `json:"resourceId"`
	AlertRuleName        string               `json:"alertRuleName"`
	PolicyName           string               `json:"policyName"`
	Resource             Resource             `json:"resource"`
	HasFinding           bool                 `json:"hasFinding"`
	ResourceRegionId     string               `json:"resourceRegionId"`
	ResourceName         string               `json:"resourceName"`
	Source               string               `json:"source"`
	RiskRating           string               `json:"riskRating"`
	ResourceRegion       string               `json:"resourceRegion"`
	PolicyDescription    string               `json:"policyDescription"`
	PolicyRecommendation string               `json:"policyRecommendation"`
	AccountId            string               `json:"accountId"`
	PolicyId             string               `json:"policyId"`
	ResourceCloudService string               `json:"resourceCloudService"`
	CloudType            string               `json:"cloudType"`
	AlertTS              int64                `json:"alertTs"`
	ComplianceMetadata   []ComplianceMetadata `json:"complianceMetadata"`
	CallbackURL          string               `json:"callbackUrl"`
	AlertId              string               `json:"alertId"`
	PolicyLabels         []string             `json:"policyLabels"`
	ResourceType         string               `json:"resourceType"`
}

type Resource struct {
	Data               Data     `json:"data"`
	ResourceTs         int8     `json:"resourceTs"`
	URL                string   `json:"url"`
	RRN                string   `json:"rrn"`
	AccountId          string   `json:"accountId"`
	CloudAccountGroups []string `json:"cloudAccountGroups"`
	ResourceTags       string   `json:"resourceTags"`
	InternalResourceId string   `json:"internalResourceId"`
	RegionId           string   `json:"regionId"`
	CloudType          string   `json:"cloudType"`
	ResourceAPIName    string   `json:"resourceApiName"`
	Name               string   `json:"name"`
	AdditionalInfo     string   `json:"additionalInfo"`
	Id                 string   `json:"id"`
	Region             string   `json:"region"`
	Account            string   `json:"account"`
	ResourceType       string   `json:"resourceType"`
}

type Data struct {
	ZoneSeparation bool   `json:"zoneSeparation"`
	Kind           string `json:"kind"`
	ProjectNumber  int64  `json:"projectNumber"`
	LocationType   string `json:"locationType"`
	ACL            []struct {
		Bucket      string      `json:"bucket"`
		Role        string      `json:"role"`
		Kind        string      `json:"kind"`
		Etag        string      `json:"etag"`
		Id          string      `json:"id"`
		ProjectTeam ProjectTeam `json:"projectTeam"`
		Entity      string      `json:"entity"`
		SelfLink    string      `json:"selfLink"`
	}
	SelfLink             string           `json:"selfLink"`
	StorageClass         string           `json:"storageClass"`
	DefaultEventBaseHold bool             `json:"defaultEventBaseHold"`
	Iam                  Iam              `json:"iam"`
	Name                 string           `json:"name"`
	IamConfiguration     IamConfiguration `json:"iamConfiguration"`
	Etag                 string           `json:"etag"`
	Location             string           `json:"location"`
	TimeCreated          TimeCreated      `json:"timeCreated"`
	Id                   string           `json:"id"`
}

type ProjectTeam struct {
	ProjectNumber string `json:"projectNumber"`
	Team          string `json:"team"`
}

type Iam struct {
	ResourceID string     `json:"resourceId"`
	Kind       string     `json:"kind"`
	Bindings   []Bindings `json:"bindings"`
	Etag       string     `json:"etag"`
	Version    int8       `json:"version"`
}

type Bindings struct {
	Role    string   `json:"role"`
	Members []string `json:"members"`
}

type IamConfiguration struct {
	BucketPolicyOnly         BucketPolicyOnly         `json:"bucketPolicyOnly"`
	UniformBucketLevelAccess UniformBucketLevelAccess `json:"uniformBucketLevelAccess"`
}

type BucketPolicyOnly struct {
	Enabled bool `json:"enabled"`
}

type UniformBucketLevelAccess struct {
	Enabled bool `json:"enabled"`
}

type TimeCreated struct {
	DateOnly      bool  `json:"dateOnly"`
	TimeZoneShift int8  `json:"timeZoneShift"`
	Value         int64 `json:"value"`
}

type ComplianceMetadata struct {
	SystemDefault   bool   `json:"systemDefault"`
	RequirementName string `json:"requirementName"`
	CustomAssigned  bool   `json:"customAssigned"`
	StandardName    string `json:"standardName"`
	ComplianceId    string `json:"complianceId"`
	SectionId       string `json:"sectionId"`
	RequirementId   string `json:"requirementId"`
}

type OpsGeneieCreateAlertRequest struct {
	Message     string       `json:"message"`
	Alias       string       `json:",omitempty"`
	Description string       `json:"description"`
	Responders  []*Responder `json:"responders,omitempty"`
	VisibleTo   []*VisibleTo `json:"visibleTo,omitempty"`
	Actions     []string     `json:"actions,omitempty"`
	Tags        []string     `json:"tags,omitempty"`
	Details     *Details     `json:",omitempty"`
	Entity      string       `json:",omitempty"`
	Priority    string       `json:"priority,omitempty"`
}

type Responder struct {
	Id       string `json:"id,omitempty"`
	Name     string `json:",omitempty"`
	Username string `json:"username,omitempty"`
	Type     string `json:"type"`
}

type VisibleTo struct {
	Id       string `json:"id,omitempty"`
	Name     string `json:"name,omitempty"`
	Username string `json:"username,omitempty"`
	Type     string `json:"type"`
}

type Details struct {
	Key1 string `json:"key1"`
	Key2 string `json:"key2"`
}

type OpsGenieRequest struct {
	Action string `json:"action"`
	Alert  Alert  `json:"alert"`
}

type Alert struct {
	AlertId         string       `json:"alertId"`
	Message         string       `json:"message"`
	Tags            []string     `json:"tags"`
	TinyId          string          `json:"tinyId"`
	Entity          string       `json:"entity"`
	Alias           string       `json:"alias"`
	CreateAt        int64        `json:"createAt"`
	UpdatedAt       int64        `json:"updatedAt"`
	Username        string       `json:"username"`
	UserId          string       `json:"userId"`
	Description     string       `json:"description"`
	Team            string       `json:"team"`
	Responders      []Responders `json:"responders"`
	Teams           []string     `json:"teams"`
	Actions         []string     `json:"actions"`
	Source          string       `json:"source"`
	IntegrationName string       `json:"integrationName"`
	IntegrationId   string       `json:"integrationId"`
	IntegrationType string       `json:"integrationType"`
}

type Responders struct {
	Id   string `json:"id"`
	Type string `json:"type"`
	Name string `json:"name"`
}

type Source struct {
	Name string `json:"name"`
	Type string `json:"type"`
}
