package url_checker

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)




type URLRequest struct {
	URL string `json:"url"`
}
func UrlChecker(w http.ResponseWriter,r *http.Request){

	u := URLRequest{}

	if err := json.NewDecoder(r.Body).Decode(&u); err != nil{
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp, err :=http.Get(u.URL)
	if err != nil{
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if resp.StatusCode != http.StatusOK{
		http.Error(w,
			errors.New(fmt.Sprintf("non-200 error code received %d", resp.StatusCode)).Error(),
			http.StatusInternalServerError)
		return
	}

	fmt.Println(w, "url is up")
}

